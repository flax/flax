/*
format a date as DATE_MED  'Oct 14, 1983' 
dependencies: luxon
*/



const { DateTime } = require("luxon");


// \get the date with luxon (for all date)
  eleventyConfig.addFilter("postDate", (dateObj) => {
    let date = new Date(dateObj)
    return DateTime.fromJSDate(date).toLocaleString(DateTime.DATE_MED);
  });
